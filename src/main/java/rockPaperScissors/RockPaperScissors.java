package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        String humanChoice;
        String aiChoice;
        Random rand = new Random();
        boolean exit = false;

        
        while(true) {
            System.out.printf("Let's play round %d\n", roundCounter);
            while(true) {
                humanChoice = readInput("Your choice (Rock/Paper/Scissors)? ").toLowerCase();
                if(rpsChoices.contains(humanChoice)) {
                    break;
                }
                else {
                    System.out.printf("I do not understand %s. Could you try again?\n", humanChoice);
                }
            }
            aiChoice = rpsChoices.get(rand.nextInt(rpsChoices.size()));
            if(humanChoice.equals(aiChoice)) {
                System.out.printf("Human chose %s, computer chose %s. It's a tie!\n", humanChoice, aiChoice);
            }
            else if((humanChoice.equals("rock") && aiChoice.equals("scissors")) || (humanChoice.equals("scissors") && aiChoice.equals("paper")) || (humanChoice.equals("scissors") && aiChoice.equals("paper"))) {
                System.out.printf("Human chose %s, computer chose %s. Human wins!\n", humanChoice, aiChoice);
                ++humanScore;
            }
            else {
                System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", humanChoice, aiChoice);
                ++computerScore;
            }
            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);
            while(true) {
                humanChoice = readInput("Do you wish to continue playing? (y/n)? ");
                if(humanChoice.equals("y")) {
                    ++roundCounter;
                    break;
                }
                else if(humanChoice.equals("n")) {
                    System.out.println("Bye bye :) \n");
                    exit = true;
                    break;
                }
                else {
                    System.out.printf("I do not understand %s. Could you try again?\n", humanChoice);
                }
            }
            if(exit) {
                break;
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
